FROM debian:9 as build

ENV LUAJIT_LIB /usr/local/lib
ENV LUAJIT_INC /usr/local/include/luajit-2.0

RUN apt update && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g-dev libssl-dev
RUN wget http://luajit.org/download/LuaJIT-2.0.4.tar.gz && tar xvfz LuaJIT-2.0.4.tar.gz	&& rm LuaJIT-2.0.4.tar.gz && cd LuaJIT-2.0.4 && make && make install
RUN wget https://github.com/vision5/ngx_devel_kit/archive/v0.2.19.tar.gz -O ngx_devel_kit-0.2.19.tar.gz && tar xvfz ngx_devel_kit-0.2.19.tar.gz && rm ngx_devel_kit-0.2.19.tar.gz
RUN wget https://github.com/openresty/lua-nginx-module/archive/v0.10.15.tar.gz -O lua-nginx-module-0.10.15.tar.gz && tar xvfz lua-nginx-module-0.10.15.tar.gz && rm lua-nginx-module-0.10.15.tar.gz 
RUN wget 'https://nginx.org/download/nginx-1.13.6.tar.gz' && tar xvfz nginx-1.13.6.tar.gz && rm nginx-1.13.6.tar.gz 
WORKDIR /nginx-1.13.6
RUN ./configure --prefix=/nginx --with-ld-opt="-Wl,-rpath,${LUAJIT_LIB}" --add-module=/ngx_devel_kit-0.2.19 --add-dynamic-module=/lua-nginx-module-0.10.15 && make -j2 && make install


FROM debian:9
WORKDIR /nginx
COPY --from=build /nginx .   
RUN  chmod +x /nginx/sbin/nginx
RUN ln -s /nginx/sbin/nginx /usr/local/sbin/nginx
WORKDIR /nginx/html
EXPOSE 80
EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]







